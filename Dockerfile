FROM golang:latest as builder

WORKDIR /build
COPY ./* ./
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -o /external-secrets-client

FROM alpine:latest

COPY --from=builder /external-secrets-client /external-secrets-client
RUN chown nobody:nobody /external-secrets-client
RUN chmod 550 /external-secrets-client
RUN chmod u+s /external-secrets-client
RUN chmod g+s /external-secrets-client
# Remove all executables except the service
# RUN RUN rm -Rf /bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/local/sbin
CMD ["/external-secrets-client"]
