package main

import (
	"fmt"
	"os"

	"gitlab.com/keymandll/secretsource"
)

func main() {
	secretSource, err := secretsource.SecretSource(os.Getenv("SECRET_ARN"))
	if err != nil {
		fmt.Printf("Failed to initialize secret source handler: %s\n", err.Error())
	}
	// Service drops privileges using the `DropPrivileges` function exposed
	// by the `secretsource` library so it will not be able to read the mounted
	// AWS Web Identity token.
	// Please note that the `SECRET_USER_UID` and `SECRET_USER_GID` environment
	// variables are primarily used by the secretsource library.
	err = secretsource.DropPrivileges()
	if err != nil {
		fmt.Printf("Failed to switch to drop privileges: %s\n", err.Error())
		os.Exit(1)
	}

	// Fetch secret from AWS SM. The secretsource library handles escalating
	// and dropping privileges as required.
	printIds("beforeFetchSecret")
	secret, err := secretSource.Get()
	if err != nil {
		fmt.Printf("Failed to fetch secret: %s\n", err.Error())
		os.Exit(2)
	}
	printIds("afterFetchSecret")
	fmt.Printf("Secret from AWS SM: %s\n", string(secret))

	// ------------------------------------------------------------------------
	// PLAYGROUND
	// ------------------------------------------------------------------------
	// To see what the service can see while running as the non-privileged user
	// we do a `setreuid` and `setregid`. Setting the effective [u|g]id
	// only is not enough because the shell, being a new process, would be
	// running with the privileges of the privileged user. (This also means you
	// should try really hard not to have arbitrary code injection and command
	// execution vulnerabilities in your app/service.)
	// So, here's a shell to experience how much the service can access while
	// running as the unprivileged (e.g. `nobody`) user.
	fmt.Println("Executing bind shell...")
	err = dropPrivilegesForever()
	if err != nil {
		fmt.Printf("Failed to drop all privileges: %s\n", err.Error())
		os.Exit(1)
	}
	shell()

	secretSource.Close()
}
