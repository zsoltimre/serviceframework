package main

import (
	"fmt"
	"os"
	"strconv"
	"syscall"
)

func dropPrivilegesForever() error {
	uid := os.Getenv("SECRET_USER_UID")
	gid := os.Getenv("SECRET_USER_GID")
	if len(uid) == 0 || len(gid) == 0 {
		return fmt.Errorf("The SECRET_USER_UID and SECRET_USER_GID environment variables must be set.\n")
	}
	userId, err := strconv.Atoi(uid)
	if err != nil {
		return fmt.Errorf("Failed to resolve UID: %s\n", err.Error())
	}
	groupId, err := strconv.Atoi(gid)
	if err != nil {
		return fmt.Errorf("Failed to resolve GID: %s\n", err.Error())
	}
	err = syscall.Setregid(groupId, groupId)
	if err != nil {
		return fmt.Errorf("Failed to set rGID: %s\n", err.Error())
	}
	err = syscall.Setreuid(userId, userId)
	if err != nil {
		return fmt.Errorf("Failed to set rUID: %s\n", err.Error())
	}
	return nil
}

func printIds(title string) {
	fmt.Printf("%s:\n", title)
	fmt.Printf("\trUID: %d, rGID: %d\n", syscall.Getuid(), syscall.Getgid())
	fmt.Printf("\teUID: %d, eGID: %d\n", syscall.Geteuid(), syscall.Getegid())
}
