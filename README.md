# Secrets Management in a AWS/Kubernetes Environment

This repository is a small demo environment to show how to best handle application/service secrets in an [AWS](https://aws.amazon.com/)/[Kubernetes](https://kubernetes.io/) environment from a security point of view. Please note that I will demonstrate everything using Kubernetes in AWS, specifically [EKS](https://aws.amazon.com/eks/). You can achieve the same with other cloud providers as well.The fundamental techniques are all related to basic operating system features and software.

The following diagram illustrates what we are about to implement.

![Secrets Management Implementation](./docs/secrets-management-impl.png)

You can find the [Rust](https://www.rust-lang.org/) version of this code (without the `secretsource` piece) in the [rust branch](https://gitlab.com/keymandll/external-secrets-client/-/tree/rust?ref_type=heads).

## Current Problems

Applications operate with secrets. Making sure these secrets are well protected is of paramount importance. Below are two common examples of how secrets are used in an AWS environment.

 * Most of the applications and services (referring to them as applications in the remainder of this document) deployed in AWS use several AWS services. Accordingly, some secret is shared with the containers that allow the applications (most likely the AWS SDK used by the applications) to authenticate to these AWS services.
 * Often, applications have to communicate with each other. As authentication is a must, applications have at least one more secret, which is used to authenticate to another application.

Secrets are most often shared with applications using environment variables or mounted secret files.

The [CIS Kubernetes Benchmark](https://www.cisecurity.org/benchmark/kubernetes) highlighted that it's relatively common for developers to expose environment variables in logs for debugging purposes, therefore sharing secrets with applications using environment variables is discouraged by the CIS Kubernetes Benchmark. The only reason mounted secret files seem to be more secure is that their content is not in environment variables, thus, won't get logged. But, developers logging environment variables is not the only threat to secrets. Secrets, whether in environment variables or mounted secrets files, can be exposed in several ways, such as memory leaks, local file inclusion, certain injection attacks, arbitrary command execution or code injection. And, of course, whoever has access to the containers runtime (whether legitimate access or not) can easily access these secrets, even though they should not be able to, especially not in production environments.

The CIS Kubernetes Benchmark mentions that we should consider using external secret storage, and I completely agree with that. What I have concerns about is how the industry approaches external secrets: with not much care. I say this because, external secret storage or not, the result is the same: the secrets end up in files or environment variables. But having a "database" for your secrets so you can manage them centrally is not the only goal of external secret storage. You can build on the concept and protect your secrets better.

The industry's approach to managing externally stored secrets is by using solutions such as the [External Secrets Operator](https://external-secrets.io/v0.8.1/) to fetch secrets and distribute those to the various applications and service. While these solutions make dealing with external secrets convenient, there are are some issues with the approach.

The following table summarises the problems with the external secrets management solutions adopted by the industry and how the proposed solution addresses them. Please note we will be focusing on managing application/service secrets only.

| Problem | Solution |
| ------- | -------- |
|  A single component handles all the secrets of every application and service in your environment. A problem with this component would impact all applications and services. | Applications fetch their own secrets from the external secret storage, this way eliminating a single point of failure. |
| A component that should not have access to application secrets in the first place handles all the secrets. If this component gets compromised, all the secrets are exposed, meaning all applications and services are at risk. | The applications fetch their own secrets from the external secret storage, thus, if the application or the container running the application gets compromised, the attacker can only access the secret of the compromised application. The secrets of other applications are not present, thus not at risk. |
| In a production environment, people do not need to access application/service secrets. At the same time, secrets show up in the memory space of the solution chosen to fetch and distribute application secrets; they are included in mounted secret files or environment variables within containers, present in the applications' memory, and they may accidentally end up in logs. Sometimes, people decide to store secrets used to initialise secret objects in the external secret store in Git repositories in clear-text form or encrypted. This extended exposure allows for plenty of opportunities for unauthorised access. Therefore, keeping secrets confidential throughout their lifecycle is challenging. | The proposed solution implements secrets management in a way that application secrets are at no point known to people. In addition, the secrets are confined into the memory space of the application and will not show up anywhere else. This drastically limits opportunities for access. |
| Secrets should be rotated periodically to limit unauthorised access to services in case one or more secrets get compromised. As they are not aware of the application state, existing solutions must periodically (and frequently) poll the secrets manager to see if the secrets need to be refreshed and the new secrets distributed to the services. Any solution unaware of the application's state and the state of the secret(s) costs more financially and performance-wise. | The proposed solution allow fetching secrets only when they are rotated or expired. |

The above are just a few problems from the top of my head. Who knows how long this list could get.

## WARNING

<span style="color: #ff0000">**Please note that this demo service opens port `4444/tcp` and exposes a shell over it without any authentication required.**</span> This is so you can look around in the container to see what the service can and cannot access while running in the non-privileged mode.

You can follow [this Kubernetes guide](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/) to set up access to the service so you can access the shell.

```bash
kubectl port-forward -n secret ${POD_NAME} 4444:4444
```

Then, access the shell by executing the following command in another terminal:

```bash
nc -v 127.0.0.1 4444
```

## What you will see

Below is an example output of the service.

```
% kubectl logs -n secret secrettest-ff9bfd569-jstrf

beforeFetchSecret:
	rUID: 501, rGID: 20
	eUID: -2, eGID: -2
afterFetchSecret:
	rUID: 501, rGID: 20
	eUID: -2, eGID: -2
Secret from AWS SM: my-test-secret
Executing bind shell...
Listening on tcp port 4444...
```

Between `beforeFetchSecret` and `afterFetchSecret` the service switches the effective UID and effective GID to `2000` so it can access the mounted AWS Web Identity token. Once the secret is obtained from AWS SM, the service switches back to a low-privilege user that has no access to the mounted AWS Web Identity token.

## Implementation

### In-memory Only Secrets

An external secrets storage such as [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/) (AWS SM) enables us to load secrets from the store directly into the memory of our application. *Please note that I will only cover using AWS Secrets Manager (AWS SM) as external secrets storage in this document.*

One could implement a simple library similar to my [secretsource Go module](https://gitlab.com/keymandll/secretsource) to fetch secrets from AWS SM. Indeed, applications must fetch their secrets. It is the only way to keep them in memory and not expose them in files and environment variables.

To illustrate how it looks like when it is an application managing its secret, you can have a look at the [deployment.yaml](./helm/secret/templates/deployment.yaml) file and the [values.yaml](./helm/secret/values.yaml) file of the included [Helm chart](https://helm.sh/docs/topics/charts/). As you can see, we only share the secret's [ARN](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference-arns.html) with the application in the `SECRET_ARN` environment variable. The application then uses the ARN to fetch the secret from AWS SM.

### External Secrets Storage

Let's look at another set of credentials we must deal with.

An application that manages its secrets has to authenticate to the secrets storage, meaning another set of credentials is required: the AWS authentication credentials. We could share the AWS authentication credentials with our service via environment variables or mounted secret files, but we would not want to bother with that. Fortunately, there's a more convenient solution: ServiceAccounts can assume [IAM roles](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html).

If you look at the [serviceaccount.yaml](./helm/secret/templates/serviceaccount.yaml) file, you can see that the service account is annotated with an IAM role, namely `eks.amazonaws.com/role-arn`. As the name suggests, the role allows reading secrets. This role has a policy attached that only allows reading secrets. We want to be as strict as possible; thus, we should also specify which secrets (by ARN) the service account should be able to read.

I'm not going to cover how to set up a service account to assume an IAM role, but you can find links to the relevant documentation from Amazon below:

 1. [Configuring a Kubernetes service account to assume an IAM role](https://docs.aws.amazon.com/eks/latest/userguide/associate-service-account-role.html)
 2. [Configuring pods to use a Kubernetes service account](https://docs.aws.amazon.com/eks/latest/userguide/pod-configuration.html)

If you read through the above AWS resources, you may find an interesting environment variable called [AWS_WEB_IDENTITY_TOKEN_FILE](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) being involved. The environment variable points to a file that contains an OAuth 2.0 access token or OpenID Connect ID token. This is the secret (within the file) the AWS SDK will pick up automatically and use to authenticate to AWS services, in our case, to AWS SM. As we can see, this is ultimately a mounted secret file as recommended by the CIS Benchmark.

![Assumed Role Mounted Secret](./docs/assumed-role-mounted-secret.png)

It isn't an in-memory-only secret. Are we back to square one? Not exactly.

The AWS Web Identity token in the mounted file must be readable by the application and, therefore, will also be readable to anyone with access to the container, whether that access is authorised or unauthorised. Of course, the token expires after a while, but as long as someone has access to the container will also have access to the refreshed tokens. Again, suppose the application suffers from memory leaks, local file inclusion, certain injection attacks, arbitrary command execution or code injection. In that case, an attacker can obtain the token and will continue to be able to as long as the vulnerability/vulnerabilities allowing access exist.

Fortunately, we can drastically reduce the attack surface.

### Seteuid and setegid

The idea is simple: only give the application access to the AWS credential mounted as a secret file when needed. Whenever the application is not communicating with AWS services, it should have no access to the file. We can achieve this by using [seteuid](https://man7.org/linux/man-pages/man2/seteuid.2.html) and [setegid](https://man7.org/linux/man-pages/man3/setegid.3p.html). Check out [user.go](./user.go) to see how its used.

In [deployment.yaml](./helm/secret/templates/deployment.yaml), under `securityContext`, we specified that the service should run with UID and GID set to `2000`. If we look into the running container, we can see that the mounted secret file was configured so that only the user with the ID `2000` can read it.

![AWS Mounted Secret Permissions](./docs/aws-secret-file-permissions.png)

When our service starts up, it will do so with UID `2000`; thus, it can access the content of the mounted secret file. If the service sets its effective UID to the UID, for example, of the `nobody` user and the effective GID to `nobody`, it can no longer read the file's content. It requires the following for the service to be able to switch to `nobody` and back to UID `2000`:

 1. The service executable should be owned by `nobody`, and the setuid bit must be set. If we want to be able to switch the group as well, the setgid bit must be set too. See the [Dockerfile](./Dockerfile) at lines 11-14.
 2. The user with UID `2000` must be able to execute the executable
 3. `runAsUser` and `runAsGroup` must be set to `2000` in the pod's `securityContext` so the service starts with UID and GID `2000`.
 4. `allowPrivilegeEscalation` in the `securityContext` of the pod must be set to `true` (the default value) for `seteuid` and `setegid` to work. More on this later.

Peeking into the container, you can see how the filesystem permissions of the `external-secrets-client` were set up:

```
/run/secrets/eks.amazonaws.com/serviceaccount $ ls -l /external-secrets-client 
-r-sr-sr-x    1 nobody   nobody     9813272 Apr 20 23:18 /external-secrets-client
```

Finally, the service should call `sete[u|g]id` to switch to `nobody` immediately and only switch back to `2000:2000` when interacting with an AWS service, in this case, with AWS SM. You can see this implemented and explained in the `service` function of [main.go](./main.go).

The diagram below illustrates the process.

![Switching Users](./docs/seteuid-process-diagram.png)

As can be seen, the application can only access the mounted AWS token when needed. This phase was marked with red in the diagram. Of course, the closer we implement the `sete[u|g]id` calls to using the AWS SDK calls, the better.

### securityContext / fsGroup

As you can see [here](./helm/secret/templates/deployment.yaml) on line 17, setting `fsGroup` undermines the security of our implementation because it does not only influence file permissions but it makes the user a member of the group as well:

> Since fsGroup field is specified, **all processes of the container are also part of the supplementary group ID 2000**."

https://kubernetes.io/docs/tasks/configure-pod-container/security-context/

Setting`fsGroup` to `2000` would allow the service to access the AWS token even after the service `sete[u|g]id`'ed to `nobody`. Therefore, we must not use `fsGroup`.

But why do we have `fsGroup`? More importantly, why is it under `securityContext`?

### securityContext / allowPrivilegeEscalation

We had to ensure that `allowPrivilegeEscalation` was set to `true` to use `seteuid`. The [Kubernetes documentation](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) also covers this and says:

![Allow Privilege Escalation](./docs/allow-privesc.png)

Quoting from the [documentation](https://www.kernel.org/doc/Documentation/prctl/no_new_privs.txt) of `no_new_privs`:

> With no_new_privs set, execve promises not to grant the privilege to do anything that could not have been done without the execve call. **For example, the setuid and setgid bits will no longer change the uid or gid; file capabilities will not add to the permitted set**...

Well, we disabled this protection. But, we can take alternative measures to prevent privilege escalation. For example:

 * Using a tiny Docker image like [Alpine](https://hub.docker.com/_/alpine).
 * Using a read-only file system. (`securityContext/readOnlyRootFilesystem`)
 * Not allowing executables in read-write areas (such as mounted volumes). Standard practice would be to set `noexec`, `nosuid` and `nodev` options on volume mounts, especially on tmpfs ones.
 * Not having setuid/setgid binaries around on the filesystem.

In general, follow security best practices and hardening guides for your operating system.

## Application Secrets Creation and Rotation

If you remember the pretty diagram about the implementation earlier, you can see that I included an AWS Lambda function in there. While this repository does not include anything that involves AWS lambda, there's a reason why it's in the diagram.

There are two common scenarios:

  1) People expose secrets in clear-text form in Git repositories.
  
  2) More security-aware people were committing PGP-encrypted secrets into Git repositories. 

Scenario #1 is very dangerous. Scenario #2 is an unnecessary burden.

If you use an external secrets manager, you likely already have automation to create the secret objects.

How the implementation of that automation would look like with my proposed setup is that it processes a simple yaml file similar to the one shown below.

```yaml
secrets:
  - name: ApplicationSecretOne
    rotationInterval: 8h
    region: eu-west-1
  - name: ApplicationSecretTwo
    rotationInterval: 3h
    region: eu-east-2
```

The automation would call an AWS lambda function for each secret defined in the yaml file so that the lambda function would:

 * Create the secret object in AWS Secrets Manager, and
 * generate a strong, random initial value for the new secret
 * return **only the secret's ARN** to your automation

This way, no one knows the value of the applications' secrets because they are not exposed anywhere. You would pass the appropriate secrets' ARN to the services, similar to what was discussed earlier, so the applications can go and fetch them. Therefore, only the services that need them would know the secrets. That is precisely how it should be.

## Profit

 1. Application secrets are fetched by the applications themselves. As a result there is not a single component that has access to the secrets of all applications and only the applications know the secrets obtained from AWS SM.
 2. As secrets obtained from AWS SM are only show up in the memory of the applications it is a lot less likely they get accidentially logged.
 3. Using lambda functions to generate the initial value for the secrets stored in AWS SM and to also use lambda functions to perform secret rotation, there is no need to store secrets in Git repositories or anywhere else outside of AWS SM.
 4. Less likely the AWS Web Identity token gets exposed because the application only has access to it for short period of times. Please note certain application vulnerabilities (especially code injection and command injection) can provide access to the AWS Web Identity token for attacker.
 5. Possibility to fetch secrets only when they expired (assuming they expire), this way things get cheaper. Unlike with e.g. [https://github.com/external-secrets/external-secrets](https://github.com/external-secrets/external-secrets) that must poll secrets from AWS SM periodically, thus costs a lot more.

# TODOs

 1. How to set `noexec`, `nosuid` and `nodev` options on `emptyDir` volume mounts?
